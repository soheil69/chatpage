package com.example.myapplication.domain.api

import com.example.myapplication.domain.model.Chat

interface DataProvider {

    suspend fun provideNextChatList():List<Chat>
}