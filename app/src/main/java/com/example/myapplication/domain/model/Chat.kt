package com.example.myapplication.domain.model

import java.util.Date

data class Chat(
    val id: Int,
    val text: String,
    val date: Date,
    val sendByMe: Boolean
)
