package com.example.myapplication.app.adapters.chat

import java.util.Date

data class ChatItem(
    val id: Int,
    val text: String,
    val date: Date,
    val sendByMe: Boolean
)
