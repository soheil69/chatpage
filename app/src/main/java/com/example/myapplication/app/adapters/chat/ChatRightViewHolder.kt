package com.example.myapplication.app.adapters.chat

import android.icu.text.SimpleDateFormat
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.myapplication.core.base.BaseViewHolder
import com.example.myapplication.databinding.ItemChatRightBinding
import java.util.Locale

class ChatRightViewHolder(parent: ViewGroup): BaseViewHolder<ItemChatRightBinding>(
    ItemChatRightBinding.inflate(LayoutInflater.from(parent.context), parent, false)
) {
    private val sdf = SimpleDateFormat("HH:mm", Locale.getDefault())

    fun bind(chat: ChatItem){
        binding.text.text = chat.text
        binding.date.text = sdf.format(chat.date)
    }
}