package com.example.myapplication.app.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.myapplication.domain.model.Chat
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class HomeFragment : Fragment(), HomeViewHandlerDelegate {
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var viewHandler: HomeViewHandler

    private val viewModel: HomeViewModel by lazy {
        ViewModelProvider(this)[HomeViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        viewModel.loadChatDataList()
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.state.onEach { renderState(it) }.collect()
            }
        }
    }

    private fun renderState(state: HomeState){
        when(state){
            is HomeState.Initial -> renderInitialState(state.chats)
        }
    }

    private fun renderInitialState(chats:List<Chat>){
         viewHandler.submitChats(chats)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = let {
        viewHandler = HomeViewHandler(inflater, container, savedInstanceState)
        viewHandler.delegate = this
        viewHandler.initRecyclerView()
        viewHandler.getRoot()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (::viewHandler.isInitialized)
            viewHandler.saveBundle(outState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewHandler.destroy()
    }



    override fun onLoad() {
        viewModel.loadChatDataList()
    }

    companion object {
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"

        @JvmStatic
        fun newInstance(param1: String, param2: String) = HomeFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_PARAM1, param1)
                putString(ARG_PARAM2, param2)
            }
        }
    }
}

