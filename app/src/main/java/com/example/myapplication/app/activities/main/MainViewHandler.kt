package com.example.myapplication.app.activities.main

import android.os.Bundle
import android.view.LayoutInflater
import com.example.myapplication.core.base.ViewHandler
import com.example.myapplication.databinding.ActivityMainBinding

class MainViewHandler(
    inflater: LayoutInflater, savedInstanceState: Bundle?
) : ViewHandler<ActivityMainBinding>(
    ActivityMainBinding.inflate(inflater), savedInstanceState
)
