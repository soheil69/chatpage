package com.example.myapplication.app.fragments.home

import com.example.myapplication.domain.model.Chat

sealed interface HomeState {
    companion object{
        fun getInitialState() = Initial(emptyList())
    }

    data class Initial(val chats: List<Chat>): HomeState

    fun appendChats(chats: List<Chat>): HomeState{
        return when(this){
            is Initial -> Initial(this.chats + chats)
        }
    }
}