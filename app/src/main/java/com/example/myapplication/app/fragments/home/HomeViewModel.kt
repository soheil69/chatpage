package com.example.myapplication.app.fragments.home

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.data.DataProviderFactory
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlin.math.log

class HomeViewModel: ViewModel() {

    private val dataProvider by lazy {
        DataProviderFactory().create()
    }

    private val mutableState:MutableStateFlow<HomeState> by lazy {
        MutableStateFlow(HomeState.getInitialState())
    }
    val state: StateFlow<HomeState> by lazy { mutableState.asStateFlow() }

    fun loadChatDataList() = viewModelScope.launch {
        val chats = dataProvider.provideNextChatList()
        mutableState.value = mutableState.value.appendChats(chats)
    }

}