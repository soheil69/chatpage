package com.example.myapplication.app.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.app.adapters.chat.ChatAdapter
import com.example.myapplication.app.adapters.chat.ChatLoadListener
import com.example.myapplication.app.mapper.toChatItem
import com.example.myapplication.core.base.ViewHandler
import com.example.myapplication.databinding.FragmentHomeBinding
import com.example.myapplication.domain.model.Chat

class HomeViewHandler(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?,
) : ViewHandler<FragmentHomeBinding>(
    FragmentHomeBinding.inflate(inflater, container, false),
    savedInstanceState
){
    companion object{
        private const val PREFETCH_SIZE = 10
    }
    private var chatAdapter: ChatAdapter? = null
    private var chatLoadListener: ChatLoadListener? = null
    var delegate: HomeViewHandlerDelegate? = null

    fun initRecyclerView() {
        chatAdapter = ChatAdapter()
        val layoutManager = LinearLayoutManager(
            binding.root.context, LinearLayoutManager.VERTICAL, true
        ).apply {
            stackFromEnd = false
            reverseLayout = true
        }
        chatLoadListener = ChatLoadListener(layoutManager, PREFETCH_SIZE){
            delegate?.onLoad()
        }
        binding.chatRecycler.layoutManager = layoutManager
        binding.chatRecycler.adapter = chatAdapter
        binding.chatRecycler.addOnScrollListener(chatLoadListener!!)
    }

    fun submitChats(chats: List<Chat>){
        chatAdapter?.submitList(chats.map { it.toChatItem() })
    }

    override fun onDestroy() {
        binding.chatRecycler.adapter = null
        binding.chatRecycler.layoutManager = null
        chatLoadListener?.let { binding.chatRecycler.removeOnScrollListener(it) }
        super.onDestroy()
    }
}