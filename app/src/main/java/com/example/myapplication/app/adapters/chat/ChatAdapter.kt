package com.example.myapplication.app.adapters.chat

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class ChatAdapter : ListAdapter<ChatItem, RecyclerView.ViewHolder>(
    ChatItemDiffUtil()
) {
    object ViewTypes {
        const val SEND_MESSAGE = 0
        const val RECEIVE_MESSAGE = 1
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position)?.let {
            when (it.sendByMe) {
                true -> ViewTypes.SEND_MESSAGE
                false -> ViewTypes.RECEIVE_MESSAGE
            }
        } ?: super.getItemViewType(position)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val chat = getItem(position)
        when (holder) {
            is ChatLeftViewHolder -> holder.bind(chat)
            is ChatRightViewHolder -> holder.bind(chat)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ViewTypes.SEND_MESSAGE -> ChatRightViewHolder(parent)
            ViewTypes.RECEIVE_MESSAGE -> ChatLeftViewHolder(parent)
            else -> error("'viewType' = $viewType is not defined!")
        }
    }
}