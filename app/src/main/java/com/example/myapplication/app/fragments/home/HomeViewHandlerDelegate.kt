package com.example.myapplication.app.fragments.home

interface HomeViewHandlerDelegate {
    fun onLoad()
}