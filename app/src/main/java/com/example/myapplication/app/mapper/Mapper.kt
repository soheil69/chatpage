package com.example.myapplication.app.mapper

import com.example.myapplication.app.adapters.chat.ChatItem
import com.example.myapplication.domain.model.Chat

fun Chat.toChatItem(): ChatItem {
    return ChatItem(id, text, date, sendByMe)
}