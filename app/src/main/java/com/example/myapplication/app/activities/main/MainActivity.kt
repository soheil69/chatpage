package com.example.myapplication.app.activities.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myapplication.R
import core.views.util.instancestate.InstanceStateHandler

class MainActivity : AppCompatActivity() {

    private lateinit var viewHandler: MainViewHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewHandler = MainViewHandler(layoutInflater, savedInstanceState)
        setContentView(viewHandler.getRoot())
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (::viewHandler.isInitialized)
            viewHandler.saveBundle(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewHandler.destroy()
    }
}