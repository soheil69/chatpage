package com.example.myapplication.app.adapters.chat

import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ChatLoadListener(
    private val layoutManager: LinearLayoutManager,
    private val prefetchSize: Int,
    private val onLoad: () -> Unit
) : RecyclerView.OnScrollListener() {

    private var isLoadEnded = false

    fun finishLoading() {
        isLoadEnded = true
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)


        if (dy < 0 && !isLoadEnded) {
            val visibleItemCount = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount
            val pastItemsVisible = layoutManager.findFirstVisibleItemPosition()
            Log.d("soheil", "onScrolled: $visibleItemCount, $totalItemCount $pastItemsVisible")
            Log.d("soheil", "${visibleItemCount + pastItemsVisible + prefetchSize} >= $totalItemCount")
            if (visibleItemCount + pastItemsVisible + prefetchSize >= totalItemCount) {
                onLoad()
            }
        }
    }
}