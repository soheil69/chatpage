package com.example.myapplication.data.generator

import java.util.Date

internal data class ChatSchema(
    val id: Int,
    val text: String,
    val date: Date,
    val sendByMe: Boolean
)
