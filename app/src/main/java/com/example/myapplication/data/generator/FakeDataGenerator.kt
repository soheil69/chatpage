package com.example.myapplication.data.generator

import kotlinx.coroutines.delay
import java.util.Date
import java.util.Random

internal class FakeDataGenerator {
    private val sampleCount = 50
    private val random = Random()
    private val fakeTexts = listOf(
        "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
        "There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...",
        "Contrary to popular belief, Lorem Ipsum is not simply random text.",
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
        "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "This book is a treatise on the theory of ethics, very popular during the Renaissance.",
        "The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.",
        "Lorem Ipsum"
    )
    private val oneYearInMillis = 31_536_000_000
    private val currentTime = Date().time
    private val dateRange = (currentTime - oneYearInMillis)..(currentTime + oneYearInMillis)

    private var lastGeneratedId = 0

    suspend fun generateFakeChatList():List<ChatSchema> {
        delay(500)
        return (0..<sampleCount).fold(mutableListOf()){ chatList, _ ->
            val text = fakeTexts[random.nextInt(fakeTexts.size)]
            val date = Date(dateRange.random())
            val sendByMe = (0..1).random().let {
                when(it){
                    0 -> false
                    else -> true
                }
            }

            chatList.apply {
                add(ChatSchema(lastGeneratedId++, text, date, sendByMe))
            }
        }
    }
}