package com.example.myapplication.data

import com.example.myapplication.data.generator.FakeDataGenerator
import com.example.myapplication.data.mapper.toDomain
import com.example.myapplication.domain.api.DataProvider
import com.example.myapplication.domain.model.Chat
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class DataProviderImpl: DataProvider {

    companion object {
        var ioDispatcher: CoroutineDispatcher = Dispatchers.IO
    }
    private val dataGenerator = FakeDataGenerator()


    override suspend fun provideNextChatList(): List<Chat> = withContext(ioDispatcher) {
        dataGenerator.generateFakeChatList().map { it.toDomain() }
    }
}