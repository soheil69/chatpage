package com.example.myapplication.data.mapper

import com.example.myapplication.data.generator.ChatSchema
import com.example.myapplication.domain.model.Chat

internal fun ChatSchema.toDomain(): Chat{
    return Chat( id, text, date, sendByMe)
}