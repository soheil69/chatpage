package com.example.myapplication.data

import com.example.myapplication.domain.api.DataProvider

class DataProviderFactory {

    companion object{
        private val dataProvider : DataProvider by lazy {
            DataProviderImpl()
        }
    }

    fun create(): DataProvider{
        return dataProvider
    }
}